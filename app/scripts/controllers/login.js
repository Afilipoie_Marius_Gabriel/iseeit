'use strict';
angular.module('iSEEit')
	.controller('LoginCtrl', function ($scope, $http, $window, $location) {
	  	$scope.firstTab = true;
	  	$scope.secondTab = false;
	  	$window.sessionStorage.setItem('user','');
		$scope.submitLogin = function () {
			$scope.submitted = true;
			if ($scope.loginForm.$valid) {
				$location.path("/dashboard");
			} else { return; }
		};
		$scope.selectTab = function () {
  			$location.path("/register");
	  	};
	});
