'use strict';
angular.module('iSEEit')
	.controller('RegisterCtrl', function ($scope, $http, $window, $location) {
	  	$scope.firstTab = false;
	  	$scope.secondTab = true;
	  	$scope.nextRegister = false;
	  	$scope.countryActive = false;
	  	$window.sessionStorage.setItem('user','');

	  	$scope.selectTab = function () {
  			$location.path("/login");
	  	};

		$scope.nextReg = function () {
			$scope.next = true;
			if ($scope.registerForm.$valid) {
				$scope.nextRegister = true;
			} else { return; }
		};

		$scope.register = function () {
			$scope.submitted = true;
			if ($scope.registerNextForm.$valid) {
				$window.sessionStorage.setItem('user',JSON.stringify( $scope.firstName ) );
				$location.path("/dashboard");
			} else { return; }
		};

		$scope.cancel = function () {
			$scope.nextRegister = false;
		};

		$scope.setPassFlag = function (value) {
			if (value.length > 8) {
                $scope.strength = 'strong';
            } else if (value.length >= 6) {
                $scope.strength = 'medium';
            } else {
                $scope.strength = 'weak';
            }
		};

		$scope.getCountry = function () {
			$scope.countryActive = !$scope.countryActive;
		};
		$scope.setCountry = function(country) {
			$scope.isCountry = country;
			$scope.countryActive = false;
		};
	});
