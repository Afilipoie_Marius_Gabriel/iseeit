'use strict';
angular.module('iSEEit')
  	.controller('DashboardCtrl' , function ($scope,$window,$location) {
  		if ($window.sessionStorage.getItem('user')) {
  			$scope.user = $window.sessionStorage.getItem('user').replace(/^"(.+(?="$))"$/, '$1');
  		} else {
  			$scope.user = 'Allready registered user!';
  		}
  	});
