'use strict';
angular.module('iSEEit', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
      .when('/login',  {
        templateUrl: 'views/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'login'
      })
      .when('/register',  {
        templateUrl: 'views/register.html',
        controller: 'RegisterCtrl',
        controllerAs: 'regiser'
      })
      .when('/dashboard', {
        templateUrl: 'views/dashboard.html',
        controller: 'DashboardCtrl',
        controllerAs: 'dashboard'
      })
      .otherwise({
        redirectTo: '/login'
      });
  }])
  .controller('HeaderCtrl', function ($scope, $window, $location) {
    console.log("HeaderCtrl");
  });
